import json

from botocore.exceptions import ClientError

from exceptions import SmsException
from service import send_open_sms, send_template_sms

from utils import response_nok, is_authenticated, response_ok

routes = {
    '/send_open_sms': {'POST': {'func': send_open_sms, 'status': 201}},
    '/send_template_sms': {'POST': {'func': send_template_sms, 'status': 201}}
}

def lambda_handler(event, context):
    print(json.dumps(event))

    if not is_authenticated(event["headers"]):
        return response_nok(401, "Não autorizado")

    try:
        body = ''
        if event['body'] is not None:
            body = json.loads(event['body'])

        query_parameter = event['queryStringParameters']

        resource = event['resource']
        method = event['httpMethod']

        if method == 'GET':
            return response_ok(routes[resource][method]['func'](query_parameter), routes[resource][method]['status'])
        else:
            return response_ok(routes[resource][method]['func'](body), routes[resource][method]['status'])
    except SmsException as lme:
        print(f"[!!] lambda_handler - SmsException - {lme.message}")
        return response_nok(lme.http_code, lme.message)
    except ClientError as ce:
        print(f"[!!] lambda_handler - ClientError - {ce}")
        return response_nok(500, ce.response['Error']['Message'])
    except (TypeError, KeyError) as e:
        print(e)
        return response_nok(400, "Bad Request")
    except Exception as e:
        print(e)
        return response_nok(500, "Internal Server Error")

# with open('scripts/json_test.json') as event_json:
#     data = json.load(event_json)
#
# lambda_handler(event=data, context=None)
