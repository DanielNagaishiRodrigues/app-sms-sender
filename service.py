#!/usr/bin/python3
import json
import os
import uuid
from string import Template

from pip._vendor import urllib3

from utils import Templates

ZENVIA_HOST = os.environ['ZENVIA_HOST']
ZENVIA_AUTH = os.environ['ZENVIA_AUTH']
SMS_FROM = os.environ['SMS_FROM']
SMS_CALLBACK = os.environ['SMS_CALLBACK']

http = urllib3.PoolManager()

headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': ZENVIA_AUTH
}


def send_open_sms(sms_to_send):
    try:
        body_json = body(sms_to_send)
        print(body_json)
        response = http.request('POST', ZENVIA_HOST,
                                headers=headers,
                                body=body_json)
    except Exception as e:
        print("[!!] Nao foi possivel enviar o SMS: {}".format(e))
        return 500

    status = response.status
    print(response.data.decode('utf-8'))
    return status


def body(sms_to_send):
    sms_data = {
        "sendSmsRequest": {
            "from": sms_to_send['from'] if 'from' in sms_to_send else SMS_FROM,
            "to": sms_to_send['phone_number'],
            "msg": sms_to_send['message'],
            "callbackOption": SMS_CALLBACK,
            "id": uuid.uuid1().__str__(),
            "flashSms": False
        }
    }

    if 'send_schedule' in sms_to_send:
        sms_data["sendSmsRequest"].update({"schedule": sms_to_send['send_schedule']})
    return json.dumps(sms_data).encode('utf-8')


def body_template(sms_template_to_send):
    msg_template = Template(str(Templates[sms_template_to_send['template']].value))
    msg_data = sms_template_to_send['msg_data']
    msg_template.substitute(nf_link=msg_data['nf_link'], nf_code=msg_data['nf_code'],
                            survey_link=msg_data['survey_link'], survey_code=msg_data['survey_code'])

    sms_data = {
        "sendSmsRequest": {
            "from": sms_template_to_send['from'] if 'from' in sms_template_to_send else SMS_FROM,
            "to": sms_template_to_send['phone_number'],
            "msg": msg_template.substitute(nf_link=msg_data['nf_link'], nf_code=msg_data['nf_code'],
                                           survey_link=msg_data['survey_link'], survey_code=msg_data['survey_code']),
            "callbackOption": SMS_CALLBACK,
            "id": uuid.uuid1().__str__(),
            "flashSms": False
        }
    }

    if 'send_schedule' in sms_template_to_send:
        sms_data["sendSmsRequest"].update({"schedule": sms_template_to_send['send_schedule']})
    return json.dumps(sms_data).encode('utf-8')


def send_open_sms(sms_to_send):
    try:
        body_json = body(sms_to_send)
        print(body_json)
        response = http.request('POST', ZENVIA_HOST,
                                headers=headers,
                                body=body_json)
    except Exception as e:
        print("[!!] Nao foi possivel enviar o SMS: {}".format(e))
        return 500

    status = response.status
    print(response.data.decode('utf-8'))
    return status


def send_template_sms(sms_template_to_send):
    try:
        body_json = body_template(sms_template_to_send)
        print(body_json)
        response = http.request('POST', ZENVIA_HOST,
                                headers=headers,
                                body=body_json)
    except Exception as e:
        print("[!!] Nao foi possivel enviar o SMS: {}".format(e))
        return 500

    status = response.status
    print(response.data.decode('utf-8'))
    return status
