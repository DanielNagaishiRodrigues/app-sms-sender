import json
import os
from enum import Enum


def is_authenticated(event_headers):
    return not (event_headers is None or
                'x-api-token' not in event_headers.keys() or
                event_headers['x-api-token'] != os.getenv('TOKEN_API'))


def jsonify_body(body):
    return json.dumps(body)


def response_nok(status_code, message):
    return response(status_code, {"message": message})


def response_ok(payload, http_status):
    return response(http_status, payload)


def response(status_code, body):
    return {"statusCode": status_code, "body": jsonify_body(body), "isBase64Encoded": False}


class Templates(Enum):
    completo = 'Link para acessar a nota fiscal do Burger King: $nf_link\nCódigo de acesso: $nf_code\n\nLink para a pesquisa: $survey_link\nCódigo para a pesquisa: $survey_code\n\nLink para download do APP BK: http://onelink.to/f5kjnw'
    sem_app = 'Link para acessar a nota fiscal do Burger King: $nf_link\nCódigo de acesso: $nf_code\n\nLink para a pesquisa: $survey_link\nCódigo para a pesquisa: $survey_code'
