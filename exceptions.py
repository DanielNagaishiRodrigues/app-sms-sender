from utils import response


class SmsException(Exception):

    def __init__(self, http_code, message):
        self.http_code = http_code
        self.message = message
        super(SmsException, self).__init__(message)

    def to_http_response(self):
        print("[ERROR] {} -> {}".format(self.http_code, self.message))
        return response(self.http_code, {'message': self.message})


class SmsNotValid(SmsException):
    def __init__(self):
        super(SmsNotValid, self).__init__(400, "Localização inválida!")


class SmsNotFound(SmsException):
    def __init__(self):
        super(SmsNotFound, self).__init__(400, "Localização não encontrada!")


class RequestInvalid(SmsException):
    def __init__(self, message):
        super(RequestInvalid, self).__init__(400, message)
